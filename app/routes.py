from flask import render_template, flash, redirect, g
from app import app
import sqlite3

DATABASE = "app/database.db"

def get_db(): 
    db = getattr(g, '_database', None)
    if db is None: 
        g._database = sqlite3.connect(DATABASE)
        db = g._database
    return db

@app.teardown_appcontext
def close_connection(exception): 
    db = getattr(g, '_database', None)
    if db is not None: 
        db.close()

#################### Home
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='My Smart Organizer')

#################### Recipes
@app.route('/recipes')
def recipes(): 
    curseur = get_db().cursor()
    vegetarian_courses = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE recipe_cat='Plat' AND
    vegetarien IS TRUE""")]
    vegan_courses = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE recipe_cat='Plat' AND
    vegan IS TRUE""")]
    main_courses = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE 
    recipe_cat='Plat'""")]
    desserts = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE 
    recipe_cat='Dessert'""")]
    sauces = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE 
    recipe_cat='Sauce'""")]
    pates_pains = [i for i in curseur.execute("""SELECT recipe_name, recipe_slurm FROM Recipes WHERE 
    recipe_cat='Pates et pains'""")]
    print(main_courses)
    return render_template('recipes.html', title='Recipes', vegetarien = vegetarian_courses, vegan = vegan_courses, 
    desserts = desserts, sauces = sauces, plats = main_courses, pates_pains = pates_pains)


#################### Selected Recipe from Recipes
@app.route('/selected_recipe/<recipe_slurm>')
def select_recipe(recipe_slurm): 
    curseur = get_db().cursor()
    recipe_name = list(curseur.execute(f"""SELECT recipe_name FROM Recipes WHERE recipe_slurm='{recipe_slurm}'"""))[0][0]
    recipe_ingr_data = [i for i in curseur.execute(f"""SELECT ingredient, quantity, measure FROM Ingredient_recipe WHERE 
    recipe='{recipe_name}'""")]
    recipe_instr = [i[0] for i in curseur.execute(f"""SELECT instruction FROM Instruction_recipe WHERE 
    recipe='{recipe_name}' ORDER BY number""")]
    return render_template('selected_recipe.html', recipe_name=recipe_name, recipe_slurm=recipe_slurm , recipe_ingr_data = recipe_ingr_data, recipe_instr = recipe_instr)


#################### Menu
@app.route('/menu')
def menu():
    return render_template('menu.html', title='Menu of the Week')

#################### In Season Vegetables
@app.route('/vegetables')
def vegetables(): 
    curseur = get_db().cursor()
    spring_veg = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE 
    ingredient_cat='Legume' AND spring IS TRUE""")]
    summer_veg = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Legume' AND summer IS TRUE""")]
    autumn_veg = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Legume' AND autumn IS TRUE""")]
    winter_veg = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Legume' AND winter IS TRUE""")]
    print(winter_veg)
    return render_template('vegetables.html', title='In Season Vegetables', spring_veg=spring_veg, summer_veg=summer_veg, autumn_veg=autumn_veg, winter_weg=winter_veg)

#################### In Season Fruits
@app.route('/fruits')
def fruits(): 
    curseur = get_db().cursor()
    spring_fruit = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE 
    ingredient_cat='Fruit' AND spring IS TRUE""")]
    summer_fruit = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Fruit' AND summer IS TRUE""")]
    autumn_fruit = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Fruit' AND autumn IS TRUE""")]
    winter_fruit = [i[0] for i in curseur.execute("""SELECT ingredient_name FROM Ingredients WHERE
    ingredient_cat='Fruit' AND winter IS TRUE""")]
    return render_template('fruits.html', title='In Season Fruits', spring_fruit=spring_fruit, summer_fruit=summer_fruit, autumn_fruit=autumn_fruit, winter_fruit=winter_fruit)

