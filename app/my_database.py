import sqlite3

# to connect to the database
connexion = sqlite3.connect("database.db") # if database.db does not exist, it is created
connexion.row_factory = sqlite3.Row
curseur = connexion.cursor()

#################### DATABASE CREATION

# recipe category (dessert, main course...)
curseur.execute('''CREATE TABLE IF NOT EXISTS Recipe_category(
    recipe_category TEXT, 
    recipe_category_slurm TEXT,
    PRIMARY KEY (recipe_category)
    UNIQUE (recipe_category_slurm)
)''')

# main recipe data (name, desc, ...)
curseur.execute('''CREATE TABLE IF NOT EXISTS Recipes(
    recipe_name TEXT,
    recipe_slurm TEXT,
    recipe_description TEXT, 
    recipe_cat TEXT,
    people_nb INT NOT NULL, 
    nb_instruction INT NOT NULL, 
    preparation_time INT NOT NULL,
    cuisson_time INT,
    vegetarien BOOL,
    vegan BOOL,
    PRIMARY KEY (recipe_name)
    UNIQUE (recipe_slurm)
    FOREIGN KEY (recipe_cat) REFERENCES Recipe_category(recipe_category) ON DELETE CASCADE
)''')

curseur.execute('''CREATE TABLE IF NOT EXISTS Ingredient_category(
    ing_category TEXT, 
    PRIMARY KEY (ing_category)
)''')

# ingredients used in the different recipes
curseur.execute('''CREATE TABLE IF NOT EXISTS Ingredients(
    ingredient_name TEXT,
    ingredient_name_slurm TEXT,
    ingredient_cat TEXT, 
    summer BOOL, 
    autumn BOOL, 
    winter BOOL, 
    spring BOOL, 
    PRIMARY KEY (ingredient_name)
    UNIQUE (ingredient_name_slurm)
    FOREIGN KEY (ingredient_cat) REFERENCES Ingredient_category(ing_category) ON DELETE CASCADE
)''')

curseur.execute('''CREATE TABLE IF NOT EXISTS Ingredient_recipe(
    quantity INT,
    measure TEXT, 
    ingredient TEXT, 
    recipe TEXT,
    FOREIGN KEY (ingredient) REFERENCES Ingredients(ingredient_name) ON DELETE CASCADE,
    FOREIGN KEY (recipe) REFERENCES Recipes(recipe_name) ON DELETE CASCADE
    )''')

curseur.execute('''CREATE TABLE IF NOT EXISTS Instruction_recipe(
    instruction TEXT, 
    number INT, 
    recipe TEXT, 
    FOREIGN KEY (recipe) REFERENCES Recipes(recipe_name)
)''')


##### Exemple de construction

curseur.execute("""INSERT INTO Ingredient_category VALUES
('Cereal'),
('Legumineuse'),
('Legume'),
('Fruit'), 
('Condiment'), 
('Viande'), 
('Derive animal'), 
('Autre')""")

curseur.execute("""INSERT INTO Recipe_category VALUES
('Plat', 'plat'), 
('Sauce', 'sauce'), 
('Dessert', 'dessert'), 
('Pates et pains', 'pates-et-pains')""")


##### For the recal : summer, autumn, winter, spring
curseur.execute("""INSERT INTO Ingredients VALUES
('Huile de sesame', 'huile-de-sesame', 'Autre', 1, 1, 1, 1),
('Germes de soja en boite', 'germe-de-soja-en-boite', 'Autre', 1, 1, 1, 1),  
('Bouillon cube', 'bouillon-cube', 'Condiment', 1, 1, 1, 1),
('Ail', 'ail', 'Condiment', 1, 1, 1, 1),
('Sauce soja', 'sauce-soja', 'Condiment', 1, 1, 1, 1),
('Piment doux', 'piment-doux', 'Condiment', 1, 1, 1, 1), 
('Asperge', 'asperge', 'Legume', 1, 0, 0, 0), 
('Oignon', 'oignon', 'Legume', 0, 0, 0, 1),
('Chou Pointu', 'chou-pointu', 'Legume', 0, 0, 1, 0), 
('Epinards', 'epinards', 'Legume', 0, 0, 1, 1),
('Carotte', 'carotte', 'Legume', 0, 0, 1, 0),
('Citron', 'citron', 'Fruit', 0, 0, 1, 0), 
('Pomme', 'pomme', 'Fruit', 0, 1, 1, 0),
('Poire', 'poire', 'Fruit', 0, 1, 1, 0), 
('Lardons', 'lardons', 'Viande', 1, 1, 1, 1), 
('Filet de poulet', 'filet-de-poulet', 'Viande', 1, 1, 1, 1), 
('Oeuf', 'oeuf', 'Derive animal', 1, 1, 1, 1), 
('Huile d olive', 'huile-d-olive', 'Autre', 1, 1, 1, 1), 
('Thym', 'thym', 'Condiment', 1, 1, 1, 1), 
('Herbes de provence', 'herbes-de-provence', 'Condiment', 1, 1, 1, 1), 
('Curry', 'curry', 'Condiment', 1, 1, 1, 1), 
('Noix de muscade', 'noix-de-muscade', 'Condiment', 1, 1, 1, 1),
('Peche', 'peche', 'Fruit', 1, 0, 0, 0), 
('Chataine', 'chataigne', 'Fruit', 0, 1, 0, 0), 
('Chorizo', 'chorizo', 'Viande', 1, 1, 1, 1), 
('Pate brisee', 'pate-brisee', 'Autre', 1, 1, 1, 1), 
('Pate sablee', 'pate-sablee', 'Autre', 1, 1, 1, 1), 
('Pate feuilletee', 'pate-feuilletee', 'Autre', 1, 1, 1, 1), 
('Poivron', 'poivron', 'Legume', 1, 0, 0, 0), 
('Tomate', 'tomate', 'Legume', 1, 0, 0, 0), 
('Farine de ble', 'farine-de-ble', 'Cereal', 1, 1, 1, 1), 
('Farine de ble noir', 'farine-de-ble-noir', 'Cereal', 1, 1, 1, 1), 
('Lentilles', 'lentilles', 'Legumineuse', 0, 0, 1, 0), 
('Feves', 'feves', 'Legumineuse', 1, 0, 0, 1), 
('Haricots rouges', 'haricots-rouges', 'Legumineuse', 1, 0, 0, 0), 
('Levure chimique', 'levure-chimique', 'Autre', 1, 1, 1, 1), 
('Levure boulangere', 'levure-boulangere', 'Autre', 1, 1, 1, 1), 
('Sucre', 'sucre', 'Autre', 1, 1, 1, 1), 
('Huile neutre', 'huile-neutre', 'Autre', 1, 1, 1, 1), 
('Cumin', 'cumin', 'Condiment', 1, 1, 1, 1), 
('Persil', 'persil', 'Condiment', 1, 1, 1, 1), 
('Beurre', 'beurre', 'Derive animal', 1, 1, 1, 1), 
('Lait', 'lait', 'Derive animal', 1, 1, 1, 1), 
('Fromage rape', 'fromage-rape', 'Derive animal', 1, 1, 1, 1),
('Creme epaisse', 'creme-epaisse', 'Derive animal', 1, 1, 1, 1), 
('Creme fleurette', 'creme-fleurette', 'Derive animal', 1, 1, 1, 1), 
('Yaourt', 'yaourt', 'Derive animal', 1, 1, 1, 1),
('Yaourt a la grec', 'yaourt-a-la-grec', 'Derive animal', 1, 1, 1, 1), 
('Oeuf de caille', 'oeuf-de-caille', 'Derive animal', 1, 1, 1, 1), 
('Viande hachee', 'viande-hachee', 'Viande', 1, 1, 1, 1), 
('Jambon', 'jambon', 'Viande', 1, 1, 1, 1), 
('Cuisse de poulet', 'cuisse-de-poulet', 'Viande', 1, 1, 1, 1), 
('Orange', 'orange', 'Fruit', 0, 0, 1, 0), 
('Noix', 'noix', 'Fruit', 0, 1, 1, 0), 
('Figue', 'figue', 'Fruit', 0, 1, 0, 0), 
('Framboise', 'framboise', 'Fruit', 1, 0, 0, 0), 
('Fraise', 'fraise', 'Fruit', 0, 1, 0, 0),
('Pates', 'pates', 'Autre', 1, 1, 1, 1), 
('Sel', 'sel', 'Condiment', 1, 1, 1, 1), 
('Poivre', 'poivre', 'Condiment', 1, 1, 1, 1),
('Concombre', 'concombre', 'Legume', 1, 1, 0, 0), 
('Laitue', 'laitue', 'Legume', 1, 1, 0, 1), 
('Pomme de terre', 'pomme-de-terre', 'Legume', 0, 1, 1, 0), 
('Mache', 'mache', 'Legume', 0, 0, 1, 0), 
('Navet', 'navet', 'Legume', 1, 1, 1, 1),
('Pomme de terre nouvelle', 'pomme-de-terre-nouvelle', 'Legume', 1, 0, 0, 1), 
('Jaune d oeuf', 'jaune-d-oeuf', 'Derive animal', 1, 1, 1, 1), 
('Blanc d oeuf', 'blanc-d-oeuf', 'Derive animal', 1, 1, 1, 1),
('Vinaigre de cidre', 'vinaigre-de-cidre', 'Condiment', 1, 1, 1, 1), 
('Vinaigre balsamique', 'vinaigre-balsamique', 'Condiment', 1, 1, 1, 1), 
('Haricots verts', 'haricots-verts', 'Legume', 1, 0, 0, 0)
""")

curseur.execute("""INSERT INTO Recipes VALUES
('Wok de chou pointu', 'wok-de-chou-pointu', 'Wok aux saveurs asiatiques parfait pour l hiver', 'Plat', 2, 11, 30, 30, 1, 1), 
('Gateau simple a la creme', 'gateau-simple-a-la-creme', 'Gateau simplissime a la creme', 'Dessert', 8, 10, 20, 45, 1, 1)
""")

curseur.execute("""INSERT INTO Ingredient_recipe VALUES
(1, 'cuillere a cafe', 'Piment doux', 'Wok de chou pointu'), 
(1, 'unite', 'Citron', 'Wok de chou pointu'), 
(500, 'grammes', 'Chou pointu', 'Wok de chou pointu'), 
(2, 'unites', 'Carotte', 'Wok de chou pointu'), 
(100, 'grammes', 'Garmes de soja en boite', 'Wok de chou pointu'), 
(1, 'unite', 'Ail', 'Wok de chou pointu'), 
(2, 'cas', 'Sauce soja', 'Wok de chou pointu'),
(1, 'unite', 'Bouillon cube', 'Wok de chou pointu'), 
(1, 'unite', 'Oignon', 'Wok de chou pointu'), 
(3, 'cas', 'Huile de sesame', 'Wok de chou pointu'), 
(3, 'cas', 'Huile d olive', 'Wok de chou pointu'), 
(4, 'unites', 'Oeuf', 'Gateau simple a la creme'), 
(250, 'grammes', 'Farine de ble', 'Gateau simple a la creme'),
(180, 'grammes', 'Sucre', 'Gateau simple a la creme'),
(4, 'cas', 'Sucre', 'Gateau simple a la creme'),
(300, 'mL', 'Creme fleurette', 'Gateau simple a la creme'), 
(8, 'grammes', 'Levure chimique', 'Gateau simple a la creme'), 
(500, 'mL', 'Lait', 'Gateau simple a la creme')""")

curseur.execute("""INSERT INTO Instruction_recipe VALUES
("Dans une petite poele, faire revenir pendant 3 minutes l'ail cisele." , 1, 'Wok de chou pointu'), 
("Ajouter la sauce soja, trois cas d'eau et 1 cas du citron presse.", 2, 'Wok de chou pointu'),
("Porter a ebullition le tout et reserver." , 3, 'Wok de chou pointu'),  
("Dans un wok, faire revenir l'oignon emince dans l'huile d'olive.", 4, "Wok de chou pointu"), 
("Ajouter le chou finement cisele. Faire revenir 2 minutes", 5, "Wok de chou pointu"), 
("Ajouter un bouillon cube prealablement dilue dans environ 60 cL d'eau", 6, 'Wok de chou pointu'), 
("Couvrir et laisser cuire environ 20 minutes en surveillant la cuisson", 7, 'Wok de chou pointu'), 
("Une fois le chou bien tendre, ajouter les carottes coupees en julienne", 8, 'Wok de chou pointu'), 
("Ajouter les pousses de soja et laisser cuire jusqu'a evaporation du liquide", 9, 'Wok de chou pointu'), 
("Trois minutes avant la fin de la cuisson, ajouter la marinade", 10, 'Wok de chou pointu'), 
("Servir immediatement", 11, 'Wok de chou pointu')""")

curseur.execute("""INSERT INTO Instruction_recipe VALUES
("Prechauffer le four à 160 degres.", 1, 'Gateau simple a la creme'),
("Separer le blanc des jaunes d'oeufs.", 2, 'Gateau simple a la creme'),
("Battre les blancs d'oeufs en neige avec une pincee de sel.", 3, 'Gateau simple a la creme'),
("Lorsqu'ils commencent a etre fermes, ajouter la moitie des 180 grammes de sucre.", 4, 'Gateau simple a la creme'),
("Finir de battre jusqu'a ce qu'ils soient bien fermes. Reserver.", 5, 'Gateau simple a la creme'),
("Ajouter aux jaunes le reste du sucre, l'huile et le lait.", 6, 'Gateau simple a la creme'),
("Ajouter la farine et la levure tamisees. Bien melanger.", 7, 'Gateau simple a la creme'),
("Incorporer delicatement les blancs au melange.", 8, 'Gateau simple a la creme'),
("Verser l'appareil dans un moule amovible de 25 cm de diametre.", 9, 'Gateau simple a la creme'),
("Enfourner pendant environ 45 minutes.", 10, 'Gateau simple a la creme')
""")

connexion.commit()
connexion.close()